// Dependencies
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");


// Route Dependencies
const userRoutes = require("./routes/userRoutes.js");
const productRoutes = require("./routes/productRoutes.js");


// Set-up server
const app = express();


// Middlewares
app.use(express.json());
app.use(express.urlencoded({extended: true}));


// Database connection
mongoose.connect("mongodb+srv://admin:admin1234@b256abrugena.rckx5fh.mongodb.net/B256_E-CommerceAPI?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => console.log(`Were connected to the cloud database`));


// Parent Routes
app.use("/users", userRoutes);
app.use("/products", productRoutes);

// Server listening
app.listen(4000, () => console.log(`API is now online on port 4000`))