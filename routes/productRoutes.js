// Dependencies
const express = require("express");
const router = express.Router();
const productController = require("../controllers/productControllers.js");
const auth = require("../auth.js");


// Route for creating a product
router.post("/create", auth.verify, (req, res) => {

	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin){
		productController.addProduct(data.product).then(resultFromController => res.send(resultFromController));
	}
	else{
		res.send(false);
	}
	
})


// Route for retrieving all products
router.get("/all", (req, res) => {
	productController.getAllProducts().then(resultFromController => res.send(resultFromController));
})


// Route for retrieving all active products
router.get("/activeproducts", (req, res) => {
	productController.getActiveProducts().then(resultFromController => res.send(resultFromController));
})


// Route for retrieving a single product
router.get("/:productId", (req, res) => {
	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));
})


// Route for updating a product
router.put("/update/:productId", auth.verify, (req, res) => {

	const data = {
		product : req.body,
		isAdmin : auth.decode(req.headers.authorization).isAdmin,
		params : req.params
	}

	if(data.isAdmin){
		productController.updateProduct(data.product, data.params).then(resultFromController => res.send(resultFromController));
	}

	else{
		res.send(false);
	}

})


// Route for archiving the product
router.patch("/archive/:productId", auth.verify, (req, res) => {

	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		params: req.params
	}

	if(data.isAdmin){
		productController.archiveProduct(data.product, data.params).then(resultFromController => res.send(resultFromController));
	}
	else{
		res.send(false);
	}
	
})


// Route for activating the product
router.patch("/activate/:productId", auth.verify, (req, res) => {

	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		params: req.params
	}

	if(data.isAdmin){
		productController.activateProduct(data.product, data.params).then(resultFromController => res.send(resultFromController));
	}
	else{
		res.send(false);
	}
	
})








module.exports = router;