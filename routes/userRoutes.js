// Dependencies
const express = require("express");
const router = express.Router();
const userController = require("../controllers/userControllers.js");
const auth = require("../auth.js");

// Route for user registration
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
})

// Route for user authentication
router.post("/authentication", (req, res) => {
	userController.authenticateUser(req.body).then(resultFromController => res.send(resultFromController));
})

// Route for retrieving all users
router.get("/registeredusers", auth.verify, (req, res) => {

	const data = {
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin){
		userController.allUsers().then(resultFromController => res.send(resultFromController));
	}

	else{
		res.send(false);
	}

})


// Route for retrieving a specific user
router.get("/details/:userId", auth.verify, (req, res) => {

	const data = auth.decode(req.headers.authorization);

	userController.userProfile({userId : data.id}).then(resultFromController => res.send(resultFromController));

})


// Route for changing a specific user to an Admin
router.patch("/newadmin/:id", (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin){
		userController.userAdmin(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
	}

	else{
		res.send(false);
	}
})


// Route for archiving an admin
router.patch("/archiveadmin/:id", (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin){
		userController.archiveAdmin(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
	}

	else{
		res.send(false);
	}
})


// Route for creating a product order (non-admin only)
router.post("/:id/createorder", auth.verify, (req, res) => {

	const data = {
		userId : auth.decode(req.headers.authorization).id,
		isAdmin : auth.decode(req.headers.authorization).isAdmin,
		order: req.body
	}

	if(data.isAdmin){
		res.send(false);
	}

	else{
		userController.createOrder(data).then(resultFromController => res.send(resultFromController));
	}

})


module.exports = router;