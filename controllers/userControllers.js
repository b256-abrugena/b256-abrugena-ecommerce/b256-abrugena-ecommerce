// Dependencies
const User = require("../models/User.js");
const Product = require("../models/Product.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");

// User registration
module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		firstName : reqBody.firstName,
		lastName : reqBody.lastName,
		address : reqBody.address,
		mobileNumber : reqBody.mobileNumber,
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10)
	})

	return newUser.save().then((user, err) => {
		if(err){

			return false;

		}
		else{

			return user;

		}
	})
}

// User authentication
module.exports.authenticateUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {

		if(result == null){
			return false;
		}

		else{

			const passwordDetect = bcrypt.compareSync(reqBody.password, result.password);

			if(passwordDetect){
				return {access: auth.createToken(result)}
			}
			else{
				return false;
			};

		};

	});
};


// Retrieving all users
module.exports.allUsers = () => {
	return User.find({}).then(result => {
		return result;
	});
}


// Retrieving a specific user
module.exports.userProfile = (data) => {

	return User.findById(data.userId).then((result, err) => {

		if(err){
			return false;
		}

		else{
			
			result.password = "";

			return result;

		}
		
	})
}



// Change user to Admin
module.exports.userAdmin = (paramsId, reqBody) => {

	return User.findById(paramsId).then((result, err) => {

		if(err){
			return false;
		}
		else{
			
			result.isAdmin = reqBody.isAdmin;

			return result.save().then((updateAdmin, err) =>{
				if(err){
					return false;
				}
				else{
					return updateAdmin;
				}
			})

		}

	})

};


// Archive an Admin
module.exports.archiveAdmin = (paramsId, reqBody) => {

	return User.findById(paramsId).then((result, err) => {

		if(err){
			return false;
		}
		else{
			
			result.isAdmin = reqBody.isAdmin;

			return result.save().then((updateAdmin, err) =>{
				if(err){
					return false;
				}
				else{
					return updateAdmin;
				}
			})

		}

	})

};



// Create an order (non-admin only)
module.exports.createOrder = async (data) => {
	let updatedUser = await User.findById(data.userId).then(user => {

		user.orderedProduct.push({
			products : [
				{
					productId : data.order.productId,
					productName : data.order.productName,
					quantity : data.order.quantity
				}
			],
			totalAmount : data.order.totalAmount
		});

		return user.save().then((ordered, err) => {
			if(err){
				return false;
			}
			else{
				return true;
			}
		})

	})

	let updatedProduct = await Product.findById(data.order.productId).then(product => {

		product.userOrders.push({userId : data.userId});

		return product.save().then((order, err) => {
			if(err){
				return false;
			}
			else{
				return true;
			}
		})			
	})

	if(updatedUser && updatedProduct){
		return true;
	}

	else{
		return false;
	}
}




