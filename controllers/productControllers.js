// Dependencies
const User = require("../models/User.js");
const Product = require("../models/Product.js");
const auth = require("../auth.js");


// Create a new product
module.exports.addProduct = (product) => {

	let newProduct = new Product({
		name: product.name,
		description: product.description,
		price: product.price
	})

	return newProduct.save().then((result, err) =>{
		if(err){
			return false;
		}
		else{
			return result;
		};
	});

};


// Retrieve all products
module.exports.getAllProducts = () => {
	
	return Product.find({}).then(result => {
		
		return result;

	});

};


// Retrieve active products

module.exports.getActiveProducts = () => {
	return Product.find({isActive: true}).then(result => {
		return result;
	})
};


// Retrieving a single product

module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		return result;
	})
};


// Updating a product
module.exports.updateProduct = (product, paramsId) => {
	
	let updatedProduct = {
		name : product.name,
		description : product.description,
		price : product.price
	}

	return Product.findByIdAndUpdate(paramsId.productId, updatedProduct).then((result, err) => {

		if(err){
			return false;
		}
		else{
			return result;
		}

	})
}


// Archiving a product
module.exports.archiveProduct = (product, paramsId) => {

	let inactiveProduct = {
		isActive : product.isActive
	}

	return Product.findByIdAndUpdate(paramsId.productId, inactiveProduct).then((result, err) => {

		if(err){

			return false;

		}
		else{

			return result;
		}

	})

};


// Activating a product
module.exports.activateProduct = (product, paramsId) => {

	let activatedProduct = {
		isActive : product.isActive
	}

	return Product.findByIdAndUpdate(paramsId.productId, activatedProduct).then((result, err) => {

		if(err){

			return false;

		}
		else{

			return result;
		}

	})

};



