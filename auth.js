// Dependencies
const jwt = require("jsonwebtoken");
const secret = "EcommerceAPI";

// Token Creation
module.exports.createToken = (user) => {
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	};

	return jwt.sign(data, secret, {});
};


// Token Verification
module.exports.verify = (req, res, next) => {

	let token = req.headers.authorization;

	if(typeof token !== "undefined"){

		token = token.slice(7, token.length);
		
		return jwt.verify(token, secret, (err, data) =>{
			if(err){
				return res.send(false);
			}

			else{
				next();
			}
		});
	}
	else{
		return res.send(false);
	}
};


// Token Decryption
module.exports.decode = (token) => {

	if(typeof token !== "undefined"){

		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (err, data) => {

			if(err) {
				return false;
			}
			else{
				return jwt.decode(token, {complete: true}). payload;
			}

		})

	}
	else{
		return false;
	}

}

